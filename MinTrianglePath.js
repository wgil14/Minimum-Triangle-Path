const R = require('ramda');

let readline = require('readline');
let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

let triangle = [];

rl.on('line', (line) => {

  triangle.push(line.split(' ').map(R.unary(parseInt)));

}).on('close', () => {

  console.log(`Minimal path is: ${stringSum(minimumPath(triangle))}`);

  process.exit(0);

});

/**
 * Computes the minimum path of a triangle represented as a list.
 *
 * A path through the triangle is a sequence of adjacent nodes, one from each
 * row, starting from the top. A minimal path is defined as one whose sum of
 * values in its nodes is no greater than for any other path through the triangle.
 *
 * @return {Array} The minimal path of the triangle represented as an array.
 *
 * @example
 *
 * minimumPath([[7], [6,3], [3,8 5], [11, 2, 10, 9]])
 * //=> [7, 6, 3, 11]
 *
 */
const minimumPath = triangle =>

  // Unnest the list
  R.flatten(

    // The head function returns the path from the triangle.
    R.head(

      /** Reduce the triangle from bottom to top excluding the maximum possible paths.
      Using the built-in Array.prototype.reduceRight, the initial value of the accumulator
      is optional. Thus, the last element is considered as accumulator. **/
      triangle.reduceRight(

        // Add the current row to the minimum sum of adjacents paths.
        (minimumPath, currentRow) => adjancetsWith(pairFromMinBySum, currentRow, minimumPath)
      )
    )
  )

/**
 *
 * Creates a new list out of two supplied by applying the function to each element
 * of the first list and its adjacent pair in the other list. The first list must be
 * shorter than the second one and the later, must have at least two elements.
 *
 * @param {Function} f The function used to combine the three elements into one value.
 * @param {Array} list1 The first list to consider.
 * @param {Array} list2 The second list to consider.
 * @return {Array} The list made by combining adjancets elements of `list1` and `list2`.
 *
 * @example
 *
 * var f = (x, y, z) => {
 *  // ...
 * };
 * adjancetsWith(f, [1, 2], ['a', 'b', 'c']);
 * //=> [f(1, 'a', 'b'), f(2, 'a', 'b')]
 *
 */
function adjancetsWith(f, list1, list2) {
  var adjancetsWith = [];
  var idx           = 0;
  var length        = list2.length <= 1 ? 0 : R.min(list1.length, list2.length);

  while (idx < length) {
    adjancetsWith[idx] = f(list1[idx], list2[idx], list2[idx+1]);
    idx++;
  }

  return adjancetsWith;
}

/**
 *
 * Creates a new list using the first element and the minimum value from the sum
 * of the other two. If the other elements are not lists, they are returned as
 * they are.
 *
 * @param  {Any} x The first value of the list.
 * @param  {Any} y The first value to sum and compare.
 * @param  {Any} z The second value to sum and compare.
 *
 * @return {Array} Resulting list with the first element and the minimum value from
 * the result of sum of the other elements.
 *
 * @example
 *
 * pairFromMinBySum(1, [2, 3, 4], [5, 6, 7]);
 * //=> [1, 2, 3, 4]
 */
const pairFromMinBySum = (x, y, z) => [x, R.minBy(safeDeepSum, y, z)]

/**
 *
 * Add together all elements of a list of any depth.
 * In case of input another data type, it returns the element itself.
 *
 * @param  {Any} x The list to sum.
 * @return {Any} The result of the sum or the element itself.
 *
 * @example
 *
 * safeDeepSum([1,2,3]);
 * //=> 6
 *
 * safeDeepSum(1);
 * //=> 1
 */
const safeDeepSum = (x) => Array.isArray(x) ? deepSum(x) : R.identity(x)

/**
 *
 * Performs a deep sum of all the elements of a list of any depth.
 *
 * @param  {Array} x The list to sum
 * @return {Number} The result of the sum
 *
 * @example
 *
 * deepSum([1, [2,3]]);
 * //=> 6
 */
const deepSum = (x) => R.sum(R.flatten(x))

/**
 *
 * Format a list as a sum of values.
 *
 * @param {Array} The list of numbers to sum.
 * @return {String} The formatted string.
 *
 * @example
 *
 * stringSum([7, 6, 3, 2]);
 * //=> "7 + 6 + 3 + 2 = 18"
 */
const stringSum = numbers => R.join(" + ", numbers) + (" = " + R.sum(numbers))

module.exports = minimumPath;
