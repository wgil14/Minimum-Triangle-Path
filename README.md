# Open Book Coding Exercise

A path through a triangle is a sequence of adjacent nodes, one from each row, starting from the top. So, for instance, 7 → 6 → 3 → 11 is a path down the left hand edge of the triangle.

A minimal path is defined as one whose sum of values in its nodes is no greater than for any other path through the triangle.

Please refer to `exercise.pdf` to read the exercise instructions.

## Dependencies
- Node.js v10.7.0

## Installation
To install the project dependencies, simply run `npm i` in the root folder of the project.

## Running the solution
 To run the solution, please input a text-format triangle through standard input. i.e.
```
cat data.txt | node MinTrianglePath
```

## Running the tests
 To run the tests you can execute `npm run test`
