# Assumptions

Throughout the file, I hope to leave clear any decisions made in the project.

## A note about branching

I am using only master branch to keep track of the whole project. However, this is not suitable for production software. In such scenario, I would encourage the use of another branching model like git-flow.


## Why Ramda

It seems to be that Ramda is one of the purest JavaScript functional programming libraries. It provides a huge collection of FP utilities and nothing more. It has great opinions and it avoids side-effects at all cost. Although the project is not large enough to choose a library like this, I chose it due to project assessment encourages the use of external tools.

## Imperative Style

Although functional programming is more declarative, I chose to write some functions like `adjancetsWith` in an imperative way. First, I made sure that mutations happen only in inner scopes and avoid side-effects at all cost. Second, the implementation of `adjacentsWith` is close to `R.zipWith` so I think there is no problem to write the utility to match the code-style of Ramda.


## Array.prototype.reduceRight

I preferred to use this function over `R.reduceRight` because we can skip the initial value of the reduction, keeping the code cleaner and more readable.

## The implementation

The easiest way to approach the problem is to compute every possible path and then reduce it to the minimal one. This is not only more expensive but also the bottom-to-top solution is easier to understand in the nature of functional programming. It is also easier to follow in a white-board evaluation.
