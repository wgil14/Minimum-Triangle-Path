const R = require('ramda');
const minTrianglePath = require('./MinTrianglePath.js');

describe('Minimum Triangle Path', () => {
  it('should compute the minimum path of a triangle', () => {
    const triangles = [
      {
        triangle:[[1], [1,2], [1,2,3], [1,2,3,4], [1,2,3,4,5], [1,2,3,4,5,6], [1,2,3,4,5,6,7]],
        minimumPath: 7
      },
      {
        triangle: [[7], [6,3], [3,8,5], [11,2,10,9]],
        minimumPath: 18
      },
      {
        triangle: [[1], [1,2], [1,2,3], [1,2,3,4], [1,2,3,4,5]],
        minimumPath: 5
      },
      {
        triangle: [[3], [7,4], [2,4,6], [8,5,9,3]],
        minimumPath: 16
      }
    ];

    for (let obj of triangles) {
      expect(R.sum(minTrianglePath(obj.triangle))).toBe(obj.minimumPath);
    }
  });
});
